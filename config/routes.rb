Rails.application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  resources :posts do
    resources :comments
  end

  resources :question_banks
  resources :question_categories

# Below are nested resources TODO fix them
  resources :question_sets

  resources :questions do
    resources :answers
  end

  resources :bounties
  get 'home/about'

  get 'home/contact'

  get 'home/index'

  root "home#index"

  devise_for :users, :path_prefix => 'my'
  resources :users, only: [:index, :show, :edit, :update, :destroy]
  # scope "/account" do
  #   resources :users
  # end

end
