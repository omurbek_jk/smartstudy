# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

def gen_bounties
  bounties = [10, 20, 30]
  bounties.each do |bounty|
    bount = Bounty.new
    bount.amount = bounty
    bount.save
  end
end

def gen_categories
  num_categories = 6
  category_names = ['Математика I',
                    'Математика II',
                    'Аналогия',
                    'Окуу жана тушунуу',
                    'Грамматика']
  for i in 0...num_categories do
    category = QuestionCategory.new
    category.name = category_names[i - 1]
    category.description = "#{category.name} contains only two options, It means it has only comparison.
    Select bigger or smaller one according to the QUESTION"
    category.information = " Usually you have #{i * 15} questions and #{15 * i + 10} minutes. Best way is to solve
    one by one from begining till end. Because it starts getting harder"
    category.save
  end
end

def gen_answers(qt)
  used = Time.new
  used = used.to_i % 4
  for i in 0...4 do
    ans = Answer.new
    ans.name = if qt.question_set.name.include? 'Математика'
                 Faker::Number.between(1, 1000).to_s
               else
                 "#{Faker::Lorem.sentence(2)}?"
               end
    ans.correct = true if i == used
    ans.question = qt
    ans.save

    puts "#{ans.name} - #{ans.correct}"

  end
end

def gen_questions(qset)
  ops = ['+', '-', '/', '*']
  for i in 0..20 do
    qt = Question.new
    puts qset
    puts qset.name
    if qset.name.include? 'Математика'
      qt.name = "#{Faker::Number.between(1, 1000)}#{ops[i % 4]}#{Faker::Number.between(1, 1000)} = ?"
    else
      qt.name = "#{Faker::Lorem.sentence(2)}?"
    end

    qt.explanation = Faker::Lorem.sentence(1)
    qt.question_set = qset
    qt.save

    puts "#{qt.name}"

    gen_answers(qt)

  end
end

def gen_question_sets(test_bank)
  question_sets_names = ['Математика I',
                         'Математика II',
                         'Аналогия',
                         'Окуу жана тушунуу',
                         'Грамматика']

  for i in 0...question_sets_names.length do
    qset = QuestionSet.new
    qset.name = question_sets_names[i]
    qset.instruction = Faker::Lorem.sentence(1)
    qset.guide = Faker::Lorem.sentence(3)
    qset.question_category = QuestionCategory.where('name=?', qset.name).first
    qset.question_bank = test_bank
    qset.save

    puts "#{qset.name} - #{qset.question_category}"
    gen_questions(qset)

  end
end

def gen_question_banks
  qbank_names = ['ОРТ Тест 1 (Бесплатно)',
                 'ОРТ Тест 2 (Бесплатно)',
                 'ОРТ Тест 3',
                 'ОРТ Тест 4'
                #  ,
                #  'ОРТ Тест 5',
                #  'ОРТ Тест 6',
                #  'ОРТ Тест 7',
                #  'ОРТ Тест 8',
                #  'ОРТ Тест 9',
                #  'ОРТ Тест 10'
               ]
  for i in 0...qbank_names.length do
    test_bank = QuestionBank.new
    test_bank.name = qbank_names[i]
    test_bank.bounty = i < 2 ? 0 : 15
    test_bank.save
    gen_question_sets(test_bank)
    puts "#{test_bank.name} - #{test_bank.bounty}"
  end
end

# FUNCTION CALL
gen_bounties
gen_categories
gen_question_banks
