class AddExplanationToQuestion < ActiveRecord::Migration[5.0]
  def change
    add_column :questions, :explanation, :text
  end
end
