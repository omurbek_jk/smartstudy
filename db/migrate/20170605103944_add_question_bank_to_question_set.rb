class AddQuestionBankToQuestionSet < ActiveRecord::Migration[5.0]
  def change
    add_reference :question_sets, :question_bank, foreign_key: true
  end
end
