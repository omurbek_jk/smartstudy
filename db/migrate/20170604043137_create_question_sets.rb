class CreateQuestionSets < ActiveRecord::Migration[5.0]
  def change
    create_table :question_sets do |t|
      t.text :instruction
      t.text :guide
      t.references :question_category, foreign_key: true

      t.timestamps
    end
  end
end
