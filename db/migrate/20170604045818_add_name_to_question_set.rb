class AddNameToQuestionSet < ActiveRecord::Migration[5.0]
  def change
    add_column :question_sets, :name, :text
  end
end
