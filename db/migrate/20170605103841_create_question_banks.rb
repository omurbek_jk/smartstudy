class CreateQuestionBanks < ActiveRecord::Migration[5.0]
  def change
    create_table :question_banks do |t|
      t.text :name
      t.integer :bounty, default: 0

      t.timestamps
    end
  end
end
