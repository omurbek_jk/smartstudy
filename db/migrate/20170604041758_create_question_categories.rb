class CreateQuestionCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :question_categories do |t|
      t.text :name
      t.text :description
      t.text :information

      t.timestamps
    end
  end
end
