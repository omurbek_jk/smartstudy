class AddQuestionSetToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_reference :questions, :question_set, foreign_key: true
  end
end
