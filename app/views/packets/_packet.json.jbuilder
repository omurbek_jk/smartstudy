json.extract! packet, :id, :name, :bounty, :age, :created_at, :updated_at
json.url packet_url(packet, format: :json)
