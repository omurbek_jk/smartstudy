json.extract! bounty, :id, :amount, :created_at, :updated_at
json.url bounty_url(bounty, format: :json)
