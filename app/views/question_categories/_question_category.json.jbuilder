json.extract! question_category, :id, :name, :description, :information, :created_at, :updated_at
json.url question_category_url(question_category, format: :json)
