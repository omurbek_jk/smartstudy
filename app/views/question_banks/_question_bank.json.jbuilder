json.extract! question_bank, :id, :name, :bounty, :created_at, :updated_at
json.url question_bank_url(question_bank, format: :json)
