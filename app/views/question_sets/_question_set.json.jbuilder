json.extract! question_set, :id, :instruction, :guide, :QuestionCategory_id, :created_at, :updated_at
json.url question_set_url(question_set, format: :json)
