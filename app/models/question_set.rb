# == Schema Information
#
# Table name: question_sets
#
#  id                   :integer          not null, primary key
#  instruction          :text
#  guide                :text
#  question_category_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  name                 :text
#  question_bank_id     :integer
#
# Indexes
#
#  index_question_sets_on_question_bank_id      (question_bank_id)
#  index_question_sets_on_question_category_id  (question_category_id)
#

class QuestionSet < ApplicationRecord
  belongs_to :question_category
  belongs_to :question_bank
  has_many :questions
  accepts_nested_attributes_for :questions, allow_destroy: true
end
