# == Schema Information
#
# Table name: question_categories
#
#  id          :integer          not null, primary key
#  name        :text
#  description :text
#  information :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class QuestionCategory < ApplicationRecord
  has_many :question_sets
end
