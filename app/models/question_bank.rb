# == Schema Information
#
# Table name: question_banks
#
#  id         :integer          not null, primary key
#  name       :text
#  bounty     :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class QuestionBank < ApplicationRecord
  has_many :question_sets, inverse_of: :question_bank
  accepts_nested_attributes_for :question_sets, reject_if: :all_blank, allow_destroy: true

end
