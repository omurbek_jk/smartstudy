# == Schema Information
#
# Table name: bounties
#
#  id         :integer          not null, primary key
#  amount     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Bounty < ApplicationRecord
  has_one :user
end
