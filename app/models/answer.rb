# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  name        :text
#  correct     :boolean          default(FALSE)
#  question_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_answers_on_question_id  (question_id)
#

class Answer < ApplicationRecord
  belongs_to :question
end
