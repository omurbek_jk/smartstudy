# == Schema Information
#
# Table name: questions
#
#  id              :integer          not null, primary key
#  name            :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  explanation     :text
#  question_set_id :integer
#
# Indexes
#
#  index_questions_on_question_set_id  (question_set_id)
#

class Question < ApplicationRecord
  has_many :answers
  belongs_to :question_set
end
