class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /packets
  # GET /packets.json
  def index
    @users= User.all
  end

  # GET /packets/1
  # GET /packets/1.json
  def show
  end

  # GET /packets/1/edit
  def edit
  end

  # PATCH/PUT /packets/1
  # PATCH/PUT /packets/1.json
  def update
    respond_to do |format|
      if @user.update(packet_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user}
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /packets/1
  # DELETE /packets/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to packets_url, notice: 'Packet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def packet_params
      params.require(:user).permit(:name, :bounty_id, :age, :phone, :avatar, :gender )
    end
end
